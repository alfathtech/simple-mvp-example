package com.alfathtech.simple_mvp.utils


import android.view.View
import com.alfathtech.simple_mvp.view.main.MovieAdapter


interface Callback {
    interface ViewCallback {
        fun onClick(position: Int, view: View)
    }
    interface ItemCallback {
        fun onBindView(holder: MovieAdapter.MovieHolder, position: Int)
    }
}


