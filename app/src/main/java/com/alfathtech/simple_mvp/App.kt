package com.alfathtech.simple_mvp

import android.app.Application

import com.alfathtech.simple_mvp.data.Repository

import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class App : Application() {

    internal var repository: Repository? = null

    override fun onCreate() {
        super.onCreate()

        repository = Repository(applicationContext)

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Comfortaa-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
    }

}
