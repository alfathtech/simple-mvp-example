package com.alfathtech.simple_mvp.data


import android.content.Context
import com.alfathtech.simple_mvp.data.local.SharedPrefHelper
import com.alfathtech.simple_mvp.data.model.MovieItemsResponse
import com.alfathtech.simple_mvp.data.network.APIRequest

class Repository(private val mContext: Context) {
    private val apiRequest: APIRequest
    private val prefs: SharedPrefHelper

    init {
        apiRequest = APIRequest()
        prefs = SharedPrefHelper(mContext)
    }

    fun getNowPlaying(callback: MovieItemsResponse.MovieItemsCallback) {
        apiRequest.getNowPlaying(callback)
    }

}
