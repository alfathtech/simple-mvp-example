package com.alfathtech.simple_mvp.data.network

import android.util.Log
import com.alfathtech.simple_mvp.data.model.MovieItemsResponse
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.json.JSONObject

/**
 * Created by User on 11/26/2018.
 */

class APIRequest {

    private val BASE_URL = "https://api.themoviedb.org/3/movie/"
    private val TAG = "hasil"
    private val gson: Gson

    init {
        gson = GsonBuilder().create()
    }

    fun getNowPlaying(callback: MovieItemsResponse.MovieItemsCallback) {
        val requestUrl = BASE_URL + "now_playing"

        Log.e(TAG, requestUrl)

        AndroidNetworking.get(requestUrl)
                .addQueryParameter("api_key", "3dce2cc3191c483d42c878e6409fd560")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {

                        val movieItems = gson.fromJson(response.toString(), MovieItemsResponse::class.java)

                        if (movieItems != null) {
                            callback.onSuccess(movieItems)
                        } else {
                            callback.onFailure("Cannot get Object" + ErrorCode.NOT_FOUND)
                        }
                    }

                    override fun onError(anError: ANError) {
                        callback.onFailure("Koneksi gagal" + ErrorCode.NO_INTERNET)
                    }
                })


    }


    enum class ErrorCode {
        NOT_FOUND,
        NO_INTERNET,
        SERVER_ERROR
    }


}

