package com.alfathtech.simple_mvp.view.splashscreen

import android.content.Intent
import android.os.Bundle

import com.alfathtech.simple_mvp.R
import com.alfathtech.simple_mvp.base.BaseActivity
import com.alfathtech.simple_mvp.view.main.MainActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun setUp() {

    }

    override fun setActionListener() {

    }
}
