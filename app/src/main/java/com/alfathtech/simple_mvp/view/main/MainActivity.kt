package com.alfathtech.simple_mvp.view.main

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.alfathtech.simple_mvp.R
import com.alfathtech.simple_mvp.base.BaseActivity
import com.alfathtech.simple_mvp.data.model.MovieItems
import com.alfathtech.simple_mvp.utils.Callback
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainInteractor.View {

    lateinit var movieAdapter: MovieAdapter
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUp()
        setActionListener()

        presenter.loadData()
    }


    override fun setUp() {
        presenter = MainPresenter(repository!!, this)
        movieAdapter = MovieAdapter(this)
        rv_nowplaying!!.layoutManager = LinearLayoutManager(this)
        rv_nowplaying!!.adapter = movieAdapter
    }

    override fun showLoading(isLoading: Boolean) {
        if (isLoading) {
            Toast.makeText(applicationContext, "loading", Toast.LENGTH_SHORT).show()
        } else {
//            Toast.makeText(applicationContext, "loading selesai", Toast.LENGTH_SHORT).show()
        }
    }

    override fun showError(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun upDateList(movieItems: List<MovieItems>) {
        movieAdapter.updateData(movieItems)
    }

    override fun setActionListener() {
        movieAdapter.setViewCallback(object : Callback.ViewCallback {
            override fun onClick(position: Int, view: View) {
                Toast.makeText(applicationContext, position.toString(), Toast.LENGTH_SHORT).show()
            }
        })
//        movieAdapter.setItemCallback(object : Callback.ItemCallback {
//            override fun onBindView(holder: MovieAdapter.MovieHolder, position: Int) {
//                holder.imgCover.setOnClickListener {
//                    Toast.makeText(applicationContext, position.toString(), Toast.LENGTH_SHORT).show()
//                }
//            }
//
//        })
    }

}
