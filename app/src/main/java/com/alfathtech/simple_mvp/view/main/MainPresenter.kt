package com.alfathtech.simple_mvp.view.main

import com.alfathtech.simple_mvp.data.Repository
import com.alfathtech.simple_mvp.data.model.MovieItemsResponse

/**
 * Created by User on 3/19/2019.
 */

class MainPresenter(private val repository: Repository, private val view: MainInteractor.View?) : MainInteractor.Presenter {
    override fun loadData() {

        repository.getNowPlaying(object : MovieItemsResponse.MovieItemsCallback {
            override fun onSuccess(movieItems: MovieItemsResponse) {

                if (view != null) {
                    view.showLoading(false)
                    view.upDateList(movieItems.movieItems!!)
                }
            }

            override fun onFailure(message: String) {
                if (view != null) {
                    view.showLoading(false)
                    view.showError("Gagal memuat")
                }

            }

        })
    }
}
