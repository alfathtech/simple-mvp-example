package com.alfathtech.simple_mvp.base


import android.support.v4.app.Fragment
import com.alfathtech.simple_mvp.App
import com.alfathtech.simple_mvp.data.Repository

abstract class BaseFragment : Fragment() {

    protected val repository: Repository?
        get() = (activity!!.application as App).repository

}
